path+=$HOME/.local/bin
path+=/home/front/main/script
export RUN_DIR=/home/front/main
export LESSHISTFILE=-

export STARSHIP_CONFIG=~/.config/starship/config.toml
eval "$(starship init zsh)"

cdls() {
        local dir="$1"
        local dir="${dir:=$HOME}"
        if [[ -d "$dir" ]]; then
                cd "$dir" >/dev/null; ls --color=auto
        else
                echo "bash: cdls: $dir: Directory not found"
        fi
}

alias arduino-cli="arduino-cli --config-file $XDG_CONFIG_HOME/arduino15/arduino-cli.yaml"
alias cd="cdls"
alias ls="ls --color -aAF"
alias ll="ls --color -alhAF"
alias rr="rm -rf"
alias gs="git status"
alias ga="git add"
alias grm="git rm"
alias gc="git commit -m"
alias gpush="git push"
alias gpull="git pull"
alias gi="nvim $HOME/.gitignore"
alias srczsh="source $HOME/.config/zsh/.zshrc"
