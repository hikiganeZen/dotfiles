#!/bin/sh
#Task: support for acpi

state=$( apm | grep AC | head -1 | cut -f2 -d":" | cut -c2- )
perc=$( apm | grep Remaining | head -1 | cut -f2 -d":" | cut -c2- | sed 's/.$//' )

bar0=" "
bar1=" "
bar2=" "
bar3=" "
bar4=" "

range_check() { 
	if [ $perc -ge $1 -a $perc -le $2 ]; then
		echo $3 " $perc%"
	fi
}

ramp_check() { 
	if [ $perc -ge $1 -a $perc -le $2 ]; then
		ramp_level=$3
	fi
}

ramp_inc() {
	ramp_level=$( expr $ramp_level + 1 )
        if [ $ramp_level = 4 ]; then
                echo -e "\r$bar4"
                sleep 0.5
                exit
        fi
}

ramp_print() {
	if [ $ramp_level = $1 ]; then
		echo -n $2
		sleep 0.5
		ramp_inc 
		
		echo -e "\r$3\c"
                sleep 0.5
		ramp_inc

		echo -e "\r$4\c"
                sleep 0.5
		ramp_inc

		echo -e "\r$5\c"
                sleep 0.5
		ramp_inc
	fi
}

if [ $state = "off-line" ]; then
	range_check "0" "5" $bar0
	range_check "6" "25" $bar1
	range_check "26" "50" $bar2
	range_check "51" "75" $bar3
	range_check "76" "100" $bar4

elif [ $state = "on-line" ]; then
	ramp_check "0" "5" "0"
	ramp_check "6" "25" "1"
	ramp_check "26" "50" "2"
	ramp_check "51" "75" "3"
	ramp_check "76" "100" "4"

	while true; do
		if [ $ramp_level = 4 ]; then
			exit
		fi
		ramp_print "0" $bar0 $bar1 $bar2 $bar3
		ramp_print "1" $bar1 $bar2 $bar3
		ramp_print "2" $bar2 $bar3
		ramp_print "3" $bar3
	done
fi
