#!/bin/sh

VGA_STATE=$( xrandr --query | grep VGA | awk '{ print $2 }' )

if [ "$VGA_STATE" = "connected" ]; then
	xrandr --newmode "1920x1080_60.00"  173.00  1920 2048 2248 2576  1080 1083 1088 1120 -hsync +vsync
	xrandr --addmode VGA-1 1920x1080_60.00
	xrandr --output VGA-1 --mode 1920x1080_60.00 --primary --output LVDS-1 --mode 1280x800 --right-of VGA-1 
fi
